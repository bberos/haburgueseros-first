import * as firebase from 'firebase';
    
export default firebaseConfig = {
    apiKey: "AIzaSyA6KsMEO6ucyO1a2vx5cF_C2RgomcfVNaQ",
    authDomain: "cursoexpfirebase.firebaseapp.com",
    databaseURL: "https://cursoexpfirebase.firebaseio.com",
    projectId: "cursoexpfirebase",
    storageBucket: "",
    messagingSenderId: "957573623502",
    appId: "1:957573623502:web:5b0ed5c335eb31d7"
  };
 
  firebase.initializeApp(firebaseConfig)