import React, {Component} from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import StartScreen from "../screens/Start";
import LoginScreen from "../screens/Login";
import RegisterScreen from './../screens/Register';

const RootStack = createStackNavigator(
    {
		Start: {
			screen: StartScreen
		},
		Login: {
			screen: LoginScreen
		},
		Register: {
			screen: RegisterScreen
		},
	},
	{
		initialRouteName: 'Start',
        defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: '#f4511e'
			},
			headerTitleStyle: {
                flex: 1,
				textAlign: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
				fontSize: 20,
				color: '#fff',
				fontWeight: 'bold'
			}
		}
	}
)


export default createAppContainer(RootStack);